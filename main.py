# Demonstrating testing.
# https://docs.pytest.org/en/latest/


def sum(a, b):
    return a + b


def test_sum_pos():
    assert sum(3, 5) == 8


def test_sum_neg():
    assert sum(-1, -1) == -2


# Now, do this with Gitlab CI
# http://www.patricksoftwareblog.com/setting-up-gitlab-ci-for-a-python-application/
